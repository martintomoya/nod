
Introduction
---------------

Why new Framework
````````````````````

We already have *maya.cmds* / *maya.api* and *pymel* so why do we need another framework?

There is no perfect api that does it all. **Maya.cmds** is not written in an object oriented fashion making it very clunky to work with. In the end it's just a simple translation of built-in Mel commands. The main problem is that we're dealing with strings which makes the code non-pythonic and vulnerable to scene changes.

**Pymel** on the other hand has been designed the right way, but if not handled properly it can be very heavy when dealing with a large amount of nodes. I believe this is one of the reasons larger studios abandoned it. It's very fast to write things, but not often fast to execute the code.

**Maya.api 2.0** is not as friendly to the user as pymel but it's fast and gives access to more Maya underworld.

There is one thing that none of those packages cover, which is dealing with node operations in a very readable way. Building node networks still requires several lines of code to create the node and connect its inputs and outputs, and then another node, and another node. It gets really tedious to write code building operational nodes.

So I asked myself, what if we could have a framework that would **solve all these issues** and do more, but also without having to reinvent new methods and force the user to learn yet another api from scratch. Moreover can we make it so it serves as a bridge between all the above api's and allows to be smoothly integrated with the existing code and libraries that we all have around?

What do I get
````````````````

The main power of working with **nod** is that it saves time writing, reading and updating the code. Big time!

Would you rather write:

.. code-block:: python

	addNode = mc.createNode('plusMinusAverage', n='control_offset_add')
	mc.addAttr(control, k=True, ln='offset")
	mc.setAttr(addNode + '.i1[1]',  2)
	mc.connectAttr(addNode + '.o1', 'joint_1.tx')
	mc.connectAttr(control + .'.offset', addNode + '.i1[0]')


or simply this:

.. code-block:: python

	control.offset + 2 >> joint.tx

or create an attribute, operate on it and connect the setup at once

.. code-block:: python

	control.addAttr(ln='offset', k=True) + 2 >> joint.tx


or this:

.. code-block:: python
		
	(control.spin * 2) + control.offset >> joint.rx

rather than this:

.. code-block:: python
		
	mulNode = mc.createNode('multiplyDivide', n='control_spin_mul')
	addNode = mc.createNode('plusMinusAverage', n='control_offset_sub')
	subNode = mc.createNode('plusMinusAverage', n='control_spin_mul_ox_add')
	mc.setAttr(mulNode + '.i2x', 2)
	mc.setAttr(addNode + '.op' 2)
	mc.setAttr(addNode + '.i1[1]',  10)
	mc.connectAttr(control + '.spin', mulNode + '.i1x')
	mc.connectAttr(control + '.offset', subNode + '.i1[0]')
	mc.connectAttr(mulNode + '.ox', addNode + '.i1[0]')
	mc.connectAttr(subNode + '.o1', addNode + '.i1[1]')
	mc.connectAttr(addNode + '.o1', joint + '.rx')


It gets even better when we add nod operators:

.. code-block:: python

	op.clamp((op.abs(control.stretch) * 10) + op.reverse(control.squash), min=0.1, max=control.stretchLimit) >> joint.sx


which in maya.cmds world require writing a whole essay of about *40* lines.
These **formulas** will often end up being a single line vs ~100 lines of difficult to read, write and update code.

It normally requires several lines of code to create a single operation node network:

* create utility node
* set inputs
* connect inputs
* connect outputs
* add a comment to describe the operation
  
Why not doing this in a **single line**. And even more of the operations at once still on the same line of code.

Why not just write a **formula** that would usually be our starting point before writing the code and end up being just a *comment*.
This formula will now serve both as a node **network building statement** and a *comment* at the same time, since it's self explanatory and **intuitive to read** and update if necessary.

Goals
````````` 

Here are the goals of the **nod** framework:

* Fast approach to automatically creating small and big node networks
	
	* Short **formulas**
	* Human readable
	* Easy to modify

* Mixable with existing code using *string* names or *PyNode* objects
* Allow to use cmds on Nod objects for better integration (*nod.cmds*)
* Focus purely on nodes (Use *pyside* or *pyqt* to build User Interfaces)
* Object oriented
* Bridge access to maya.api 2.0 objects


Nod Name origin
`````````````````

Technical D: The Node of Destiny
| Based on the :
`Tenacious D The Pick of Destiny <http://www.imdb.com/title/tt0365830/>`_



Development Stages
````````````````````
The nod framework was created by `Martin Orlowski <https://www.linkedin.com/in/martintomoya/>`_ and is released publicly under the `MIT License <https://gitlab.com/martintomoya/nod/blob/master/LICENSE>`_

More developers are joining soon to help to further develop the nod branch. The goal for it is to stay publicly accessible so it can be used both for personal use as well as by small and large production studios. Register your interest if you're keen to help to move the development forward.


**Stage 1** (Complete)

* Allow to write quick **formulas** to automatically build node networks
* Object oriented Nodes
* Node attributes accessible via node.property
* Production ready API
* Nod.cmds
* Documentation

**Stage 2** (In progress)

* Bridging access maya.api 2.0
* More Node types

**Stage 3**

* Solid Unit Tests coverage
* Integrated Vector Math, dealing with matrices etc.

