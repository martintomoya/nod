Connecting Nodes
-------------------

Simple syntax
```````````````

The syntax is an extension of an idea introduced in ``pymel`` and allows to write intuitive, easy to read statements.



Connect
``````````

Connections can be made using ``>>`` operator

.. code-block:: python
	
	control.spin >> joint.rx

Additionally channels can be connected to **multiple outputs at once** using ``<<`` operator.
Imagine a node with multiple output connections going outwards that is reflected in the operator shape.

.. code-block:: python
	
	control.twist << [joint1.rx, joint2.rx, joint3.rx]


Disconnect
````````````

Connected channels can be disconnected using ``!=`` operator

.. code-block:: python
	
	control.spin != joint.rx

or using ``<>`` operator in Python 2

.. code-block:: python
	
	control.spin <> joint.rx


Alternatively all connections can be broken using the *.disconnect()* method


.. code-block:: python
	
	control.twist.disconnect()



List connections
```````````````````

Get an input
'''''''''''''

.. code-block:: python
	
	joint.rx.input()


Get outputs
''''''''''''

.. code-block:: python
	
	control.twist.outputs()

Connections method
'''''''''''''''''''''

This is using the nc.listConnections function that operates on the called channel, thus using the same kwargs.

.. code-block:: python
	
	control.twist.connections()


Using nod.cmds
''''''''''''''''

.. code-block:: python
	
	nc.listConnections(control.twist)


Check connection status
`````````````````````````

There are two ways of checking if two channels are connected.

.. code-block:: python
	
	control.rx >> joint.rx

	control.rx.isConnectedTo(joint.rx)
	# True

	joint.rx.isConnectedTo(control.rx)
	# False


.. code-block:: python
	
	control.rx >> joint.rx

	control.rx in joint.rx
	# True

	joint.rx in control.rx
	# False



