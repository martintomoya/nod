Maya api 2.0 Access
----------------------

The framework makes it easy to access ``maya.api.OpenMaya`` objects directly from ``Nod`` objects.

.. note::
	
	As outlined in the :doc:`introduction` section bridging access to ``Maya api 2.0`` is a part of the Stage 2 development ongoing process.

selectionList
'''''''''''''''

.. code-block:: python

	node.selectionList()

dagPath
'''''''''''''''

.. code-block:: python

	node.dagPath

matrix
''''''''

This will return ``maya.api.OpenMaya.MMatrix`` object

.. code-block:: python

	node.matrix()

	node.matrix(exclusive=True)

	node.matrix(inverse=True)


MFn
'''''

Additionally all node types will have a quick access to the default ``MFn`` function set directly from the **Nod** object.

.. code-block:: python

	mesh.fn()
	blendShape.fn()


This is of course intuitvely chainable

.. code-block:: python

	mesh.fn().closestIntersection()

dependNode
''''''''''''

Get ``maya.api.OpenMaya.MObject`` for a deformer

.. code-block:: python

	blendShape.dependNode()
