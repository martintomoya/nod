
Renaming nodes
-------------------


When using *maya.cmds* renaming nodes outside of the scope of the code (i.e.: in Maya session) can be generous.
As objects refer to the nodes by their names they will no longer point to the right node when nodes are renamed by external methods.

*Pymel* solves this problem and always point to the originally assigned nodes however they are renamed.

:class:`nod` objects are linked up to node *uuids* under the hood which makes them equally reliable.
Additionally you can cast nodes to **Nod** objects, close your scene and reopen and the objects will still point to the right nodes.
This can be particularly useful when working on a prototype setup from a template scene and the scene has to be reopened to go back to the initial state. This does not require recasting the nodes to **Nod** objects again.


.. code-block:: python

	joint.rename('l_wing_1')

The **rename()** method has also a pretty useful feature as it supports the *python built-in* **string.replace()** method syntax. e.g.:


.. code-block:: python

	joint.rename(replace=('l_', 'r_', 1))

