
Nod
===============================
Nodes Python Framework for Maya
-------------------------------

Tired of writing hundreds of lines of code repeatedly to create node networks?
Still enjoying having to remember all the utlity nodes attribute names? Is it indexed attribute, xyz, rgb?
How about the outputs.

Start using **nod** framework to save hours of your time and make the prototyping and building processes in Maya fun again.


.. toctree::
   :maxdepth: 1

   introduction
   installation
   howto
   api_reference
   examples
   nodpy.org

Search and Index
------------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
