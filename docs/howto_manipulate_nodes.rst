
Manipulating nodes
-------------------


Nodes can be manipulated using :class:`nod.cmds`, node methods as well as ``maya.cmds``

Using nod.cmds
```````````````

:class:`nod.cmds` supports both string names and Nod objects as inputs
``maya.cmds`` will not always support the Nod objects as inputs (without converting them to string)
 
As expected the syntax for args and kwargs stays identical to the original command known to the user.

.. code-block:: python

	nc.parentConstraint(joint1, joint2)



Using object methods
`````````````````````

As expected from an Object Oriented framework, each object has a set of methods attached to it.
Refer to docs for more information.

Alternatively use python built-in functions to get more info.

.. code-block:: python

	dir(nodObject)
	help(nodObject.method)


Using maya.cmds
````````````````

Call ``node.name()`` method or convert the object to ``str(node)``

.. code-block:: python

    def push(node):
    	"""
    	External function using name strings rather than nod objects

    	Args:
    		node (str): Node name
    	"""
    	mc.setAttr(node + '.tx', 10)

    push(nodObject.name())
    # or
    push(str(nodObject))

Some ``maya.cmds`` will play well with **Nod** objects by default but it is **recommended** to either call the object's ``.name()`` or convert it to ``str()``

.. code-block:: python

    mc.parentConstraint(nodControl, nodJoint, mo=True)

    # It is however recommended to use the nod.cmds module instead
    nc.parentConstraint(nodControl, nodJoint, mo=True)


If using string formatting simply insert the nod object.

.. code-block:: python
	
	'{}.visibility'.format(nodObject)

.. note::
	
	This is for the presentation only. When working with nod objects you will not be calling nodes by strings.

	You will find yourself needing to do this only if you want to add a **nod** object
	into an External function/method.

	All nod objects are loaded with helpful methods.

You can also do the same thing with **NodChannels**

.. code-block:: python
	
	str(joint.tx)
	# 'joint1.tx'



