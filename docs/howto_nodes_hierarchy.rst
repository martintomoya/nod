
Nodes Hierarchy 
-------------------



Parenting
``````````

.. code-block:: python
	
	group = nc.createNode('transform')
	joint = nc.joint()

	joint.setParent(group)

Querying Relatives
`````````````````````

.. code-block:: python

	# Get the parent node
	joint.parent()

	# Query children directly
	# This method takes *.listRelatives* kwargs
	group.children()

	# This is also available
	group.listRelatives()


Querying Shapes
`````````````````

.. code-block:: python
	
	# Get first shape under the transform
	geometry.shape()

	# List of all shapes
	geometry.shapes()