
Accessing attributes
----------------------

All nodes come with a shortlist of predefined properties. Additionally property getter is overloaded allowing to access any node attribute via ``.`` property.

Calling an attribute on a **nod** object returns a :class:`nod.channels.base.NodChannel` object.

.. code-block:: python
	
	joint = nc.joint()
	joint.tx

Alternatively any attribute can be accessed via ``.attr()`` method.

.. code-block:: python
	
	joint.attr('translateX')

	# Attribute string with a '.' prefix is also supported
	joint.attr('.translateX')
	

This can be particularly useful when getting an attribute name as a string from a function return or when dealing with more complex attributes:

.. code-block:: python

	skinCluster = nc.createNode('skinCluster')
	skinCluster.attr('weightList[0].weights[0]').get()

Additionally one could automatically cast a *node.attribute* string to get a ``NodChannel``:

.. code-block:: python

	radius = Nod('joint.radius')
	radius.get()

Setting
````````

.. code-block:: python

	joint.tx.set(9)

	# Access parent attribute (in this case translate)
	joint.tx.parent().set(15, 30, 45)

	# Set using lists or unpacked args
	joint.r.set([30, 60, 90])
	joint.r.set(30, 60, 90)

	# Set all rotate child attributes (rx, ry, rz) to 30
	joint.r.set(30)

	# Or by casting the node.attribute string
	channel = Nod('locator.t')

	channel.set(10, 2, 3)
	channel.set([1, 2, 3])
	channel.set((1, 2, 3))


Getting
````````

.. code-block:: python

	joint.tx.get()
	joint.orient.get()

Visibility
```````````

.. code-block:: python
	
	# Short and long name
	joint.v.set(False)
	joint.visibility.set(True)

	# Handy methods will also unlock visibility channels if locked
	joint.hide()
	joint.unhide()


Adding attributes
```````````````````

Each Nod has access to an ``addAttr`` which for consistency respects identical *kwargs* as ``maya.cmds.addAttr`` command.

.. code-block:: python

	joint.addAttr(ln='kickapoo')


Locking and Hiding attributes
``````````````````````````````

.. code-block:: python

	joint.tx.lock()
	joint.t.lock()

	joint.t.unlock()

	joint.tx.hide()



