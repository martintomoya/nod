from nod.channels.base import NodChannel


class NodChannelCallableOutputs(NodChannel):

    def __init__(self, node, attributeName, parents=False):
        """
        Special subclass on NodChannel that allows an attribute to be also callable.
        In this case a list of output connected nodes will be returned when the property is called.

        e.g.: deformer.outputGeometry behaves just like any node.attribute channel but when called
              deformer.outputGeometry() it returns

        Args:
            node (nod.nodetypes.NodBase): Node
            attributeName (str): Attribute name
            parents (bool): If True return parents of the connected outputs.
                            e.g.: Parents of the connected shapes nodes (transforms)
        """
        self.node = node
        self.attributeName = attributeName
        self.parents = parents

        super(NodChannelCallableOutputs, self).__init__(node, self.attributeName)

    def get(self, value):
        super(NodChannelCallableOutputs, self).get(value)

    def __call__(self, *args, **kwargs):
        """
        Override to return a list of geometry pieces when the property is called since it's

        Returns:
            list: List of connected objects
        """
        outputs = [g.node() for g in self.node.attr(self.attributeName).outputs()]
        if not self.parents:
            return outputs

        return [o.parent() for o in outputs]