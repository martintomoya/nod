import maya.api.OpenMaya as om
import maya.api.OpenMayaAnim as oma
import maya.cmds as mc

from nod.decorators.property import DelayedProperty
from nod.channels.base import NodChannel

from nod.nodetypes.deformer import NodDeformer


class NodNonLinear(NodDeformer):

    def __init__(self, *args):
        super(NodNonLinear, self).__init__(*args)

    @DelayedProperty
    def fn(self):
        """
        Get MFnGeometryFilter object for the current deformer

        Returns:
            maya.api.OpenMaya.MFnGeometryFilter
        """
        return oma.MFnGeometryFilter(self.dependNode)
