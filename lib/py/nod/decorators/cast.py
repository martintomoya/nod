from functools import wraps


class _TypeConverter(object):

    def __init__(self):
        pass

    @classmethod
    def stringAsNod(cls, variable):
        """
        Converts the type of the variable to Nod if the variable is a string
        Returns the same value if not converting from a string

        Returns:
            nod.nodetypes.base.NodBase/nod.channels.base.NodChannel
        """
        from nod.nodetypes.base import Nod
        if not isinstance(variable, str):
            return variable

        try:
            return Nod(variable)
        except RuntimeError:
            return variable

    @classmethod
    def nodAsString(cls, variable):
        from nod.nodetypes.base import NodBase

        from nod.channels.base import NodChannel
        if not isinstance(variable, (NodBase, NodChannel)):
            return variable

        return str(variable)


def stringArgumentsAsNods(f):
    """
    Casts function arguments as Nod types
    """
    @wraps(f)
    def wrapped(*args, **kwargs):

        castArgs = [_TypeConverter.stringAsNod(x) for x in args]
        return f(*castArgs, **kwargs)

    return wrapped

def stringKeywordArgumentsAsNods(f):
    """
    Casts function kwargs as Nod types
    """
    @wraps(f)
    def wrapped(*args, **kwargs):

        castKwargs = dict( (k, _TypeConverter.stringAsNod(v)) for (k, v) in list(kwargs.items()))
        return f(*args, **castKwargs)

    return wrapped

def nodArgumentsAsStrings(f):
    """
    Casts function arguments as string types
    """
    @wraps(f)
    def wrapped(*args, **kwargs):
        castArgs = [_TypeConverter.nodAsString(x) for x in args]
        # castKwargs = dict( (k, _TypeConverter.nodAsString(v)) for (k, v) in kwargs.items())
        return f(*castArgs, **kwargs)

    return wrapped

def returnStringsAsNods(f):
    """
    Casts function returned strings as Nod(s)
    """
    @wraps(f)
    def wrapped(*args, **kwargs):
        castArgs = [_TypeConverter.nodAsString(x) for x in args]
        castKwargs = dict( (k, _TypeConverter.nodAsString(v)) for (k, v) in list(kwargs.items()))

        results = f(*castArgs, **kwargs)

        if isinstance(results, str):
            return _TypeConverter.stringAsNod(results)
        elif isinstance(results, list):
            newResults = []
            for result in results:
                if not isinstance(result, str):
                    newResults.append(result)
                    continue

                newResults.append(_TypeConverter.stringAsNod(result))
            return newResults
        return results

    return wrapped
